#include <iostream>
#include <fstream>
#include <locale>
#include  <codecvt>

using namespace std;

double rootDegree(double x, double accuracy, unsigned degree, unsigned& countOfInterations)
{
	double previous = x;
	double following = ((degree - 1) * previous + x / pow(previous, degree)) / degree;
	countOfInterations = 0;

	while (fabs(previous - following) > accuracy)
	{
		previous = following;
		following = ((degree - 1) * previous + x / pow(previous, degree - 1)) / degree;
		countOfInterations++;
	}

	return following;
}

void drawTableTop(ostream& out)
{
	unsigned degree = 3;
	out << " Enter a,b,h (type real), eps (epsilon) and degree \n";
	//cin >> begin >> end >> step >> accuracy >> degree;
	//validation
	//system("cls");
	out << "\n  Table of function's values the root of " << degree << "- power from x" << endl;
	out << " \xC9";
	out.fill('\xCD');
	out.width(6);
	out << '\xCB';
	out.width(12);
	out << "\xCB";
	out.width(12);
	out << "\xCB";
	out.width(13);
	out << "\xCB";
	out.width(26);
	out << "\xBB\n";
	out << " \xBA  x  \xBA   f(x)1   \xBA   f(x)2   \xBA  accuracy  \xBA  number of iterations  \xBa\n";
	out << " \xCC";
	out.width(6);
	out << "\xCE";
	out.width(12);
	out << "\xCE";
	out.width(12);
	out << "\xCE";
	out.width(13);
	out << "\xCE";
	out.width(26);
	out << "\xB9\n";
	out.setf(ios::fixed, ios::floatfield);
	out.fill(' ');
}

void drawTableBody(ostream& out)
{
	double begin = 1, end = 5, step = 0.5, accuracy = 0.1;
	unsigned degree = 3;
	unsigned iteration = 0;
	for (double x = begin; x <= end; x += step)
	{
		double y1 = rootDegree(x, accuracy, degree, iteration);
		double y2 = pow(x, 1. / degree);
		out << " \xBA";
		out.width(4);
		out.precision(1);
		out << x;
		out << " \xBA";
		out.width(10);
		out.precision(6);
		out << 1 / y1;
		out << " \xBA";
		out.width(10);
		out.precision(6);
		out << 1 / y2;
		out << " \xBA";
		out.width(11);
		out.precision(6);
		out << fabs(y1 - y2);
		out << " \xBA";
		out.width(13);
		out << iteration;
		out.width(13);
		out << "\xBA\n";
	}
}

void drawTableBottom(ostream& out)
{
	out.fill('\xCD');
	out << " \xC8";
	out.fill('\xCD');
	out.width(6);
	out << "\xCA";
	out.fill('\xCD');
	out.width(12);
	out << "\xCA";
	out.fill('\xCD');
	out.width(12);
	out << "\xCA";
	out.fill('\xCD');
	out.width(13);
	out << "\xCA";
	out.fill('\xCD');
	out.width(26);
	out << "\xBC\n";
}
int main()
{

	ofstream out("table.txt", ios::app);
	//const locale utf8_locale(locale(), new codecvt_utf8());
	//out << " \xC9" << "\xCD\xCD\xCD\xCD\xCD" << "\xCB" << lineLong << "\xCB" << lineLong << "\xBB\n";
	drawTableTop(out);
	drawTableTop(cout);

	drawTableBody(out);
	drawTableBody(cout);
	drawTableBottom(out);
	drawTableBottom(cout);
	out.close();
	cout << "  P r e s s   a n y   k e y   t o  E X I T . . . ";
	system("pause");
	return 0;
}