#define _CRT_SECURE_NO_WARNINGS

#include <iostream> 
#include <string>

using namespace std;

bool isLetter(const char symbol)
{
	return (symbol >= 'A' && symbol <= 'Z') || (symbol >= 'a' && symbol <= 'z');
}

char toLowerCase(const char symbol)
{
	if (symbol >= 'A' && symbol <= 'Z')
	{
		return symbol + 32;
	}
	
	return symbol;
}

bool isStringsEq(const char* str1,const char* str2, int length)
{
	bool flag = true;

	for (int i = 0; i < length; i++)
	{
		if (toLowerCase(str1[i]) != toLowerCase(str2[i]))
		{
			flag = false;
			break;
		}
	}

	return flag;
}

char ChangeCase(const char symbol)
{
	if (!isLetter(symbol))
	{
		return symbol;
	}

	if (symbol >= 65 && symbol <= 90)
	{
		return symbol + 32;
	}
	else
	{
		return symbol - 32;
	}
}

void deleteWords(char* str, const char* deleteWord)
{
	int length = strlen(deleteWord);
	char* symbols = new char[length * 2 + 1];

	for (int i = 0; i < length; i++)
	{
		symbols[i] = deleteWord[i];
		symbols[i + length] = ChangeCase(deleteWord[i]);
	}
	symbols[strlen(deleteWord) * 2] = '\0';
	
	char* pword = strpbrk(str, symbols);
	while (pword)
	{

		int pwordLength = strspn(str, symbols);

		if (length == pwordLength && strncmp(pword,deleteWord,length) == 0)
		{
			strcpy(pword, pword + length);
		}
		else 
		{
			pword += pwordLength;
		}

		pword = strpbrk(pword, symbols);
	}

}

void deleteWordsWithLength(char* str, int deleteLength)
{
	char* symbols = new char[] {"ABCDEFGHIJKLMNOPQRSTUWVXYZabcdefghijklnmopqrstuwvxyz"};
	char* pword = strpbrk(str, symbols);
	while (pword)
	{
		int length = strspn(pword, symbols);

		if (length == deleteLength)
		{
			strcpy(pword, pword + length);
		}
		else
		{
			pword += length;
		}

		pword = strpbrk(pword, symbols);
	}
}

void replaceByWord(char* str, char* deleteStr, char* replaceStr)
{
	int deleteLength = strlen(deleteStr);

	char* pword = strstr(str, deleteStr);
	while (pword)
	{
		char* temp = new char[strlen(str)];
		strcpy(temp, pword + deleteLength);
		strcpy(pword, replaceStr);
		strcat(str, temp);
		pword = strstr(str, deleteStr);
	}
	
}

int main()
{
	/*char* str = new char[256];
	cin.getline(str, 256);
	replaceByWord(str, (char*)"mam",(char*) "123543");
	cout << str;*/
}