#include "Array.h"
#include <iostream>
#include <stdexcept>

using namespace std;

const int Array::defaultCapacity = 8;

Array::Array(int count): capacity(2*count),length(0)
{
	this->pointer = allocateMemory(count);
}

Array::Array() : capacity(2*defaultCapacity),length(0)
{
	this->pointer = allocateMemory(defaultCapacity);
}

Array::Array(const Array& arr) : capacity(2 * arr.getCount()), length(arr.getCount())
{
	this->pointer = allocateMemory(arr.getCount());

	for (int i = 0; i < arr.getCount(); i++)
	{
		this->pointer[i] = arr[i];
	}

}

int Array::getCount() const
{
	return this->length;
}

void Array::push(int value)
{
	if (this->capacity == this->getCount())
	{
		resize(this->pointer, this->getCount());
		this->capacity = 2 * this->getCount();
	}
	this->pointer[this->getCount()] = value;
	this->length++;
}

void Array::pop()
{
	if (!this->getCount())
	{
		throw exception("array is empty");
	}

	this->pointer[this->getCount() - 1] = 0;
	this->length--;
}

int& Array::operator[](int index) const
{
	try {
		if (index < 0 || index > this->getCount() - 1)
		{
			throw out_of_range("n is invalid");
		}

	}
	catch (out_of_range exc)
	{
		cout << exc.what();
		exit(1);
	}

	return this->pointer[index];
}

ostream& operator<<(ostream& os,const Array& arr)
{
	os << "[";
	for (int i = 0; i < arr.getCount() - 1; i++)
	{
		os << arr[i] << ", ";

	}
	os << arr[arr.getCount()-1] <<"]";

	return os;
}

Array Array::select(predicate predicate)
{
	if (predicate == nullptr)
	{
		throw invalid_argument("predicate can not be nullptr");
	}

	int* array = allocateMemory(this->getCount()/2);
	int k = 0;
	for (int i = 0; i < this->getCount(); i++)
	{
		if (predicate((*this)[i]))
		{
			array[k++] = (*this)[i];
		}
	}

	Array result;

	for (int i = 0; i < k; i++)
	{
		result.push(array[i]);
	}

	delete[] array;

	return result;
}

int* Array::allocateMemory(int n)
{
	int* p = new int[2*n];

	for (int i = 0; i < n; i++)
	{
		p[i] = 0;
	}

	return p;
}

void Array::resize(int*& pointer, int size)
{
	int* temp = new int[size * 2]{ 0 };
	for (int i = 0; i < size; i++)
	{
		temp[i] = pointer[i];
	}
	
	delete[] pointer;
	pointer = temp;
}

Array Array::sort(comparer comparer)
{
	int* array = this->allocateMemory(this->getCount()/2);
	for (int i = 0; i < this->getCount(); i++)
	{
		array[i] = (*this)[i];
	}

	int temp;
	for (int i = 0; i < this->getCount(); i++)
	{
		for (int j = (this->getCount() - 1); j >= (i + 1); j--)
		{
			if (comparer(array[j],array[j-1]))
			{
				temp = array[j];
				array[j] = array[j - 1];
				array[j - 1] = temp;
			}
		}
	}

	Array result;

	for (int i = 0; i < this->getCount(); i++)
	{
		result.push(array[i]);
	}

	return result;
}

Array::~Array()
{
	delete[] this->pointer;
}


