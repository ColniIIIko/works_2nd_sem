﻿#pragma once
#include <iostream>
using comparer = bool(*)(int,int);
using predicate = bool(*)(int);

class Array
{
public:
	Array();
	Array(int);
	Array(const Array&);
	int getCount()const;
	void push(int);
	void pop();
	Array sort(comparer);
	Array select(predicate);
	friend std::ostream& operator <<(std::ostream&,const Array&);
	int& operator [](int) const;
	~Array();
	


private:
	static const int defaultCapacity;
	int capacity = defaultCapacity;
	int* pointer = nullptr; 
	int length = 0;
	static int* allocateMemory(int);
	static void resize(int*&,int);
};

