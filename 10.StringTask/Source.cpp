#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <cstring>
#include "String.h"

using namespace std;

void setDataToBinFile(String* words, int length , char* filename)
{
	ofstream fout(filename, ios::binary);
	if (!fout.is_open())
	{
		throw exception("Cannot open file");
	}
	for (int i = 0; i < length; i++)
	{
		int wordLength = words[i].getLength();
		fout.write((char*)&wordLength, sizeof(int));
		char* tempWord = words[i].getString();
		fout.write((char*)tempWord, sizeof(char) * wordLength);
	}
	fout.close();
}

String* getDataFromBinFile(char* filename,int& size) {
	ifstream fin(filename, ios::binary);
	size = 0;
	String* words = new String[256];
	if (!fin.is_open())
	{
		throw exception("Cannot open file");
	}
	char* temp;
	while (!fin.eof())
	{
		int length;
		fin.read((char*)&length, sizeof(int));
		temp = new char[length+1];
		fin.read((char*)temp, sizeof(char) * length);
		temp[length] = '\0';
		words[size] = String(temp);
		size++;
	}
	size--;

	return words;
}

char* getFileData(char* filename)
{
	ifstream fin(filename);
	fin.seekg (0, fin.end);
	int fileLength = fin.tellg();
	fin.seekg(0, fin.beg);
	char* filedata = new char[fileLength];
	filedata[0] = '\0';
	if (!fin.is_open())
	{
		throw exception("Cannot open file");
	}

	char* temp;
	while (!fin.eof())
	{
		temp = new char[256];
		if (fin >> temp)
		{
			strcat(filedata, temp);
			strcat(filedata, " ");
		}
		delete[] temp;
	}
	fin.close();
	return filedata;
}

int getWordAmount(char* source)
{
	char* symbols = (char*)"ABCDEFGHIJKLMNOPQRSTUWVXYZabcdefghijklnmopqrstuwvxyz";

	int wordsAmount = 0;
	char* pword = strpbrk(source, symbols);
	while (pword)
	{
		int length = strspn(pword, symbols);
		pword += length;
		pword = strpbrk(pword, symbols);
		wordsAmount++;
	}

	return wordsAmount;
}

String* ObtainWords(char* source, int n)
{
	char* symbols = (char*)"ABCDEFGHIJKLMNOPQRSTUWVXYZabcdefghijklnmopqrstuwvxyz";
	String* words = new String[n];
	char* tempWord;

	n = 0;
	char* pword = strpbrk(source, symbols);
	while (pword)
	{
		int length = strspn(pword, symbols);
		tempWord = new char[length + 1];
		strncpy(tempWord, pword, length);
		tempWord[length] = '\0';
		words[n] = String(tempWord);
		pword += length;
		pword = strpbrk(pword, symbols);
		n++;
		delete[]tempWord;
	}
	return words;
}



int main()
{
	char* text = getFileData((char*)"text.txt");
	int size = getWordAmount(text);
	String* words = ObtainWords(text, size);
	for (int i = 0; i < size; i++)
	{
		cout << words[i].getString() << " ";
	}
	setDataToBinFile(words, size, (char*)"coutmewout.bin");
	words = getDataFromBinFile((char*)"coutmewout.bin",size);
	for (int i = 0; i < size; i++)
	{
		cout << words[i].getString() << " ";
	}
}