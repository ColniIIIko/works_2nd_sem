#include "String.h"
#include <cstring>
#define _CRT_SECURE_NO_WARNINGS
using namespace std;

String::String(char* word)
{
	this->length = strlen(word);
	this->string = new char[length + 1];
	strcpy(this->string, word);
}

String::String()
{
	this->length = 0;
	this->string = nullptr;
}

String::~String()
{
	delete[] this->string;
}

char* String::getString()const
{
	return this->string;	
}

int String::getLength()const
{
	return this->length;
}

String::String(const String& str)
{
	this->length = str.getLength();
	this->string = new char[this->length + 1];
	strcpy(this->string, str.getString());
}

String String::operator=(const String& str)
{
	this->length = str.getLength();
	this->string = new char[this->length + 1];
	strcpy(this->string, str.getString());
	return str;
}
