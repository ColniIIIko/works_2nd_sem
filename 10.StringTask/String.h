#pragma once
#define _CRT_SECURE_NO_WARNINGS
class String
{
public:
	String(char*);
	String(const String&);
	String();
	~String();
	char* getString() const;
	int getLength() const;

	String operator=(const String&);

private:
	char* string;
	int length;
};

