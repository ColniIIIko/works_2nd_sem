#include <iostream>

using namespace std;

typedef double(*function)(double);
typedef double(*method)(function, double, double, int);

double rightRectIntegral(double(*function)(double), double a, double b, int n)
{
	double x = a;
	double step = (b - a) / n;
	double integral = 0;

	while (x < b)
	{
		integral += function(x);
		x += step;
	}
	integral *= step;
	return integral;
}


double avgRectIntegral(double(*function)(double), double a, double b, int n)
{
	double x = a;
	double step = (b - a) / n;
	double integral = 0;
	double max = (b * ((double)n - 1) + a) / n;
	double delta = (b - a) / 2 / n;

	while (x < max)
	{
		integral += function(x+delta);
		x += step;
	}
	integral *= step;
	return integral;
}


double calcIntegral(function func, double a, double b, double epsillon, method methodIntegral)
{
	int n = 2;
	double prevIntegral = 0;
	double nextIntegral = 1;

	while (abs(nextIntegral - prevIntegral) > epsillon)
	{
		prevIntegral = methodIntegral(func, a, b, n);
		nextIntegral = methodIntegral(func, a, b, (double)n * 2);
		n *= 2;
	}

	return nextIntegral;
}

double function1(double x)
{
	return 1. / (1 + x * x * x);
}	

	
int main()
{
	cout << calcIntegral(function1, 0, 1, .5e-4, avgRectIntegral) << endl;
	cout << calcIntegral(function1, 0, 1, .5e-4, rightRectIntegral);
}