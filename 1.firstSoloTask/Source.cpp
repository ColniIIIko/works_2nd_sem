#include <iostream>
#include <fstream>
#include <ctime>

using namespace std;

void swap(int& lhs, int& rhs)
{
	int temp = lhs;
	lhs = rhs;
	rhs = temp;
}

void deleteTheSame(int*& arr, int& size)
{
	bool* toBeDeleted = new bool[size] {false};
	int deleteCount = 0;

	for (int i = 0; i < size; i++)
	{
		if (!toBeDeleted[i])
		{
			for (int j = i+1; j < size; j++)
			{
				if (arr[i] == arr[j])
				{
					toBeDeleted[j] = true;
					deleteCount++;
				}
			}

		}
	}

	int newSize = size - deleteCount;
	int* newArr = new int[newSize];
	int temp = 0;

	for (int i = 0; i < size; i++)
	{
		if (!toBeDeleted[i] && temp<newSize)
		{
			newArr[temp] = arr[i];
			temp++;
		}
	}

	delete[] arr;
	size = newSize;
	arr = newArr;

}

int* EvenElemIndexFind(int* arr, int size, int& evenNumberCount)
{
	int* tempIndexArr = new int[size];
	int newSize = 0;					
	evenNumberCount = 0;
	for (int i = 0; i < size && arr[i] > 0; i++)
	{
		if (arr[i] % 2 == 0)
		{
			tempIndexArr[evenNumberCount] = i;
			evenNumberCount++;
		}
	}

	int* IndexArr = new int[evenNumberCount];
	for (int i = 0; i < evenNumberCount; i++)
	{
		IndexArr[i] = tempIndexArr[i];
	}

	delete[] tempIndexArr;
	return IndexArr;
}

void deleteElem(int*& deletinArr, int& size1, int* IndexArr, int size2)
{
	int newSize = size1 - size2;
	int* newArr = new int[newSize];
	bool toBeDeleted;

	int temp = 0;

	for (int i = 0; i < size1; i++)
	{
		toBeDeleted = false;

		for (int j = 0; j < size2;j++)
		{
			if (i == IndexArr[j])
			{
				toBeDeleted = true;
				break;
			}
		}

		if (!toBeDeleted)
		{
			newArr[temp] = deletinArr[i];
			temp++;
		}
	}

	delete[] deletinArr;
	deletinArr = newArr;
	size1 = newSize;

}

int* CreateNewArrWithIndex(int* arr, int* IndexArr, int IndexArrSize, int& newArrSize)
{
	newArrSize = IndexArrSize;
	int* newArr = new int[newArrSize];
	for (int i = 0; i < newArrSize; i++)
	{
		newArr[i] = arr[IndexArr[i]];
	}

	return newArr;
}

int* HexSortHelper(int* arr, int size)
{
	int mask = 0xF;
	int* HexLetterCount = new int[size]{ 0 };

	int temp;
	for (int i = 0; i < size; i++)
	{
		if (arr[i] != INT_MIN)
		{
			temp = abs(arr[i]);
		}else
		{
			HexLetterCount[i] = 0;
			continue;
		}
		for (int j = 0; j < sizeof(int) * 2 && temp >= 10; j++)
		{
			if ((temp & mask) >= 10)
			{
				HexLetterCount[i]++;
			}
			temp >>= 4;
		}
	}

	return HexLetterCount;
}

void HexSort(int* arr, int size)
{
	int* Helper = HexSortHelper(arr, size);
	bool isSwapped = true;

	for (int i = 0; i < size - 1; i++)
	{
		if (isSwapped) 
		{
			isSwapped = false;
			for (int j = 0; j < size - i - 1; j++)
			{
				if (Helper[j] > Helper[j + 1])
				{
					swap(arr[j], arr[j + 1]);
					swap(Helper[j], Helper[j + 1]);
					isSwapped = true;
				}
			}
		}
	}
}

char* toHex(int n)
{
	int mask = 0xF;
	char* Hex = new char[sizeof(int) * 2 + 1];

		for (int i = 0; i < sizeof(int) * 2; i++)
		{

			switch (n & mask)
			{
			case 0:
			{
				Hex[sizeof(int) * 2 - i - 1] = '0';
				break;
			}
			case 1:
			{
				Hex[sizeof(int) * 2 - i - 1] = '1';
				break;
			}
			case 2:
			{
				Hex[sizeof(int) * 2 - i - 1] = '2';
				break;
			}
			case 3:
			{
				Hex[sizeof(int) * 2 - i - 1] = '3';
				break;
			}
			case 4:
			{
				Hex[sizeof(int) * 2 - i - 1] = '4';
				break;
			}
			case 5:
			{
				Hex[sizeof(int) * 2 - i - 1] = '5';
				break;
			}
			case 6:
			{
				Hex[sizeof(int) * 2 - i - 1] = '6';
				break;
			}
			case 7:
			{
				Hex[sizeof(int) * 2 - i - 1] = '7';
				break;
			}
			case 8:
			{
				Hex[sizeof(int) * 2 - i - 1] = '8';
				break;
			}
			case 9:
			{
				Hex[sizeof(int) * 2 - i - 1] = '9';
				break;
			}
			case 10:
			{
				Hex[sizeof(int) * 2 - i - 1] = 'A';
				break;
			}
			case 11:
			{
				Hex[sizeof(int) * 2 - i - 1] = 'B';
				break;
			}
			case 12:
			{
				Hex[sizeof(int) * 2 - i - 1] = 'C';
				break;
			}
			case 13:
			{
				Hex[sizeof(int) * 2 - i - 1] = 'D';
				break;
			}
			case 14:
			{
				Hex[sizeof(int) * 2 - i - 1] = 'E';
				break;
			}
			case 15:
			{
				Hex[sizeof(int) * 2 - i - 1] = 'F';
				break;
			}
			}

			n >>= 4;
		}

	Hex[sizeof(int) * 2] = '\0';
	return Hex;
}

int* randomGenerateArr(int& size)
{
	srand(time(0));
	size = rand() % 11 + 10;
	int* arr = new int[size];
	for (int* p = arr; p < arr+size; p++)
	{
		*p = rand() % 51 - 5;
	}

	return arr;
}

int* setArr(int& size)
{
	srand(time(0));

	cout << "Enter size of array: ";
	cin >> size;

	system("cls");

	int* arr = new int[size];
	cout << "Enter array elements: " << endl;
	for (int i = 0; i < size; i++)
	{
		cout << i + 1 << ".";
		cin >> arr[i];
	}

	system("cls");

	return arr;
}

void getArr(int* arr, int size)
{
	for (int* p = arr; p < arr + size; p++)
	{
		cout << *p << " ";
	}
}

void Calc(int*& arr, int &size, int*& newArr, int& newSize)
{
	delete[] newArr;
	int IndexArrSize = 0;
	int* IndexArr = EvenElemIndexFind(arr, size, IndexArrSize);
	newArr = CreateNewArrWithIndex(arr, IndexArr, IndexArrSize, newSize);
	deleteElem(arr, size, IndexArr, IndexArrSize);
	delete[] IndexArr;
	deleteTheSame(arr, size);
	HexSort(arr, size);
}

char* getFileName()
{
	char* filename = new char[256];
	cout << "Enter filename :";
	cin >> filename;
	return filename;
}

void writeTofile(int* arr,int size,int* newArr,int newSize)
{
	char* filename = getFileName();
	ofstream fout(filename);
	for (int i = 0; i < size; i++)
	{
		fout << arr[i] << " ";
	}
	fout << endl;
	for (int i = 0; i < newSize; i++)
	{
		fout << newArr[i] << " ";
	}
}

void start()
{
	int switcher;
	int* arr = nullptr;
	int size = 0;
	int* newArr = nullptr;
	int newArrSize = 0;
	

	while (true)
	{
		system("pause");
		system("cls");
		cout << "1 - Random array generation" << endl;
		cout << "2 - Enter array from keyboard" << endl;
		cout << "3 - Get array" << endl;
		cout << "4 - Get secondary array" << endl;
		cout << "5 - Start program" << endl;
		cout << "6 - write arrays to file" << endl;
		cout << "0 - exit" << endl;
		cin >> switcher;

		switch (switcher)
		{
		
		case 1:
			{
				system("cls");
				delete[] arr;
				arr = randomGenerateArr(size);
				break;
			}
		case 2:
			{
				system("cls");
				delete[] arr;
				arr = setArr(size);
				break;
			}
		case 3:
			{
				system("cls");
				if (size != 0)
				{
					getArr(arr, size);
					cout << endl;
				}
				else
				{
					cout << "array is empty" << endl;
				}

				break;
			}
		case 4:
			{
				system("cls");
				if (newArrSize != 0)
				{
					getArr(newArr, newArrSize);
					cout << endl;
				}
				else
				{
					cout << "array is empty" << endl;
				}

				break;
			}
		case 5:
			{
				system("cls");
				if (size == 0)
				{
					cout << "array is empty" << endl;;
					break;
				}

				Calc(arr, size, newArr, newArrSize);
				if (arr && newArr)
				{
					cout << "Program run succsefull" << endl;
				}

				break;
			}
		case 6:
		{
			system("cls");
			if (arr && newArr) {
				writeTofile(arr, size, newArr, newArrSize);
			}
			else
			{
				cout << "Nothing to write";
			}
			break;
		}
		case 0:
			{
				exit(0);
				break;
			}

		default:
			{
				system("cls");
				cout << "Incorrect Input" << endl;
				exit(0);
				break;
			}
		}
	}
}


int main()
{
	start();
}