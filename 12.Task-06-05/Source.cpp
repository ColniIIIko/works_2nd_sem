#include <iostream>
#include <fstream>

using namespace std;

struct AmountOfValues {
	int value = INT_MIN;
	int count = 1;
};

void writeData(char* inputFile1, char* inputFile2, int* arr1,int size1, int* arr2,int size2)
{
	ofstream file1Data(inputFile1);
	ofstream file2Data(inputFile2);

	if (!file1Data.is_open() || !file2Data.is_open())
	{
		cout << "Can't open file to write!";
		return;
	}

	for (int i = 0; i < size1; i++)
	{
		file1Data << arr1[i] << " ";
	}

	for (int i = 0; i < size2; i++)
	{
		file2Data << arr2[i] << " ";
	}

	file1Data.close();
	file2Data.close();
}

int* readData(char* outputFile,int& size)
{
	ifstream fout(outputFile);
	size = 0;
	//fout.seekg(0, fout.end);
	//int length = fout.tellg()/sizeof(int);
	int* arr = new int[256];
	size = 0;
	/*fout.seekg(0, fout.beg);*/
	while (!fout.eof())
	{
		if(fout >> arr[size]) size++;
	}

	int* actualArr = new int[size];
	for (int i = 0; i < size; i++)
	{
		actualArr[i] = arr[i];
	}
	
	delete[] arr;
	fout.close();
	return actualArr;
}

AmountOfValues* resize(AmountOfValues* temp, int size)
{
	AmountOfValues* realStructArr = new AmountOfValues[size];

	for (int i = 0; i < size; i++)
	{
		realStructArr[i].value = temp[i].value;
		realStructArr[i].count = temp[i].count;
	}

	delete[] temp;

	return realStructArr;
}

AmountOfValues* generateStruct(int* arr, int size, int& realSize)
{
	AmountOfValues* structArr = new AmountOfValues[size];
	bool wasFound;
	realSize = 0;
	int offset = 0;
	for (int i = 0; i < size; i++)
	{
		wasFound = false;
		for (int j = 0; j <= i; j++)
		{
			if (structArr[j].value == arr[i])
			{
				structArr[j].count++;
				wasFound = true;
				offset++;
				break;
			}
		}

		if (!wasFound)
		{
			structArr[i-offset].value = arr[i];
			realSize++;
		}
	}

	AmountOfValues* realStructArr = resize(structArr,realSize);

	return realStructArr;
}

void setDatatoBinFile(char* inputFile, AmountOfValues* str,int size)
{
	ofstream fout(inputFile, ios::binary);
	for (int i = 0; i < size; i++)
	{
		fout.write((char*)&str[i], sizeof(AmountOfValues));
	}

	fout.close();
}

AmountOfValues* readDataFromBinFile(char* outputFile,int& size)
{
	size = 0;
	AmountOfValues* temp = new AmountOfValues[256];
	ifstream fin(outputFile, ios::binary);
	while (fin.read((char*)&temp[size], sizeof(AmountOfValues)))
	{
		size++;
	}

	AmountOfValues* realStructArr = resize(temp,size);

	fin.close();
	return realStructArr;
}

void setData(char* inputFile1, char* inputFile2, char* outputFile)
{
	ifstream file1Data(inputFile1);
	ifstream file2Data(inputFile2);
	ofstream fileOutput(outputFile);
	
	int number1 = 0, number2 = 0;
	int test;
	file1Data >> number1;
	file2Data >> number2;
	while (!file1Data.eof() || !file2Data.eof())
	{
		if (number1 <= number2)
		{
			fileOutput << number1 << " ";
			if (file1Data >> test)
			{
				number1 = test;
			}
			else
			{
				number1 = INT_MAX;
			}
		}
		if(number2 <= number1)
		{
			fileOutput << number2 << " ";
			if (file2Data >> test)
			{
				number2 = test;
			}
			else
			{
				number2 = INT_MAX;
			}
		}
	}

	if (number1 < number2)
	{
		fileOutput << number1;
	}
	else
	{
		fileOutput << number2;
	}


	file1Data.close();
	file2Data.close();
	fileOutput.close();
}

int main()
{
	//setData((char*)"inputFile1.txt", (char*)"inputFile2.txt", (char*)"outputFile.txt");
	int size;
	int structSize;
	int* arr = readData((char*)"outputFile.txt", size);
	AmountOfValues* structArr = generateStruct(arr, size, structSize);
	setDatatoBinFile((char*)"outputStruct.bin", structArr, structSize);
	int sizeStruct = 0;
	AmountOfValues* structArr2 = readDataFromBinFile((char*)"outputStruct.bin", sizeStruct);
	for (int i = 0; i < sizeStruct; i++)
	{
		cout << structArr2[i].value << " " << structArr2[i].count << endl;
	}

}