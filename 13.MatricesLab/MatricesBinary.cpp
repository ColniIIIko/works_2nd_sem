#include <iostream>
#include <fstream>

using namespace std;

void setData(char* filename,int** data,int n,int m,int size)
{
	ofstream fout(filename, ios::binary);
	if (!fout.is_open())
	{
		throw exception("cannot open file");
	}

	fout.write((char*)&n, sizeof(int));
	fout.write((char*)&m, sizeof(int));

	for (int i = 0; i < size; i++)
	{
		fout.write((char*)data[i], n * m * sizeof(int));
	}

	fout.close();
	cout << "Successfully write data to " << filename << endl;
	system("pause");
}

int** readData(char* filename,int&n, int&m, int& size)
{
	ifstream fin(filename, ios::binary);
	if (!fin.is_open())
	{
		throw exception("cannot open file");
	}
	int** data= new int* [256];
	size = 0;
	fin.read((char*)&n, sizeof(int));
	fin.read((char*)&m, sizeof(int));
	while(!fin.eof())
	{
		data[size] = new int[n * m];
		if (fin.read((char*)data[size], n * m * sizeof(int)))
		{
			size++;
		}
	}
	fin.close();

	return data;
}

bool cmpMatrixes(int* matrix1, int* matrix2,int n,int m)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if (matrix1[i * m + j] != matrix2[i * m + j])
				return false;
		}
	}

	return true;
}

void writeToFile(char* filename)
{
	int size;
	cout << "enter numbers of arrays: ";
	cin >> size;
	int** data = new int* [size];
	int n, m;
	cout << "enter array dimansions: ";
	cin >> n >> m;
	for (int i = 0; i < size; i++)
	{
		data[i] = new int[n * m];
		cout << "enter array values: ";
		for (int j = 0; j < n; j++)
		{
			for (int k = 0; k < m; k++)
			{
				cin >> data[i][m * j + k];
			}
		}
	}
	system("cls");

	setData(filename, data, n, m, size);
}

void workWithMatrixesFromFile(char* filename1,char* filename2)
{
	int size1, size2;
	int n, m;
	int** matrixes1 = readData(filename1, n, m, size1);
	int** matrixes2 = readData(filename2, n, m, size2);
	
	int minSize = min(size1, size2);

	ofstream fout2(filename2, ios::binary | ios::app);
	if (!fout2.is_open())
	{
		throw exception("cannot open file");
	}

	int flag;
	for (int i = 0; i < size1; i++)
	{
		flag = false;
		for (int j = 0; j < size2; j++)
		{
			if (cmpMatrixes(matrixes1[i], matrixes2[j], n, m))
			{
				flag = true;
				break;
			}
		}

		if (!flag)
		{
			fout2.write((char*)matrixes1[i], n * m * sizeof(int));
		}
	}
	fout2.close();
}

void displayData(char* filename)
{
	system("cls");
	int size = 0;
	int n = 0;
	int m = 0;
	int** data = readData(filename, n, m, size);
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < n; j++)
		{
			for (int k = 0; k < m; k++)
			{
				cout << data[i][m * j + k] << " ";
			}

			cout << endl;
		}
		cout << endl;
	}

	system("pause");
}

char* getFileName()
{	
	system("cls");
	char* filename = new char[256];
	cout << "Enter filename: ";
	cin >> filename;
	return filename;
}

void displayMenu()
{
	system("cls");
	cout << "1 - Init Files" << endl;
	cout << "2 - write to File" << endl;
	cout << "3 - display File" << endl;
	cout << "4 - start application" << endl;
	cout << "0 - exit" << endl;
}

int chooseFile()
{
	int choose;
	char* filename = new char[256];
	while (true)
	{
		system("cls");
		cout << "1 - Work with File1" << endl;
		cout << "2 - Work with File2" << endl;
		cout << "0 - Back" << endl;
		cin >> choose;
		if (choose == 0 || choose == 1 || choose == 2)
		{
			return choose;
		}
		else
		{
			cout << "Incorrect Input";
		}
	}
}

void menu()
{
	int choose = 0;
	char* filename1 = nullptr;
	char* filename2 = nullptr;
	enum {Exit = 0,Init, Write, Display, Start};
	while (true)
	{
		int fileNumber = 0;
		system("cls");
		displayMenu();
		cin >> choose;
		switch (choose)
		{
		case Init:
			fileNumber = chooseFile();
			if (fileNumber)
			{
				fileNumber == 1 ? filename1 = getFileName() : filename2 = getFileName();
			}
			break;
		case Write:
			fileNumber = chooseFile();
			if (fileNumber)
			{
				fileNumber == 1 ? writeToFile(filename1) : writeToFile(filename2);
			}
			break;
		case Display:
			fileNumber = chooseFile();
			if (fileNumber)
			{
				fileNumber == 1 ? displayData(filename1) : displayData(filename2);
			}
			break;
		case Start:
			if (filename1 && filename2)
			{
				workWithMatrixesFromFile(filename1, filename2);
				system("cls");
				cout << "Application run successfully";
			}
			else
			{
				cout << "Files wasn\'t initialized" << endl;
				system("pause");
			}
			break;
		case Exit:
			cout << "Shutting down the application..." << endl;
			if (filename1)
				delete[] filename1;
			if (filename2)
				delete[] filename2;
			exit(1);
		default:
			cout << "Incorrect choice";
			break;
		}
	}
}

int main()
{
	menu();
}