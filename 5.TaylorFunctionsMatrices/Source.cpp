#include <iostream>
#define PI 3.1415926535897932384626

using namespace std;

double sinTh(double parametr, double epsillon)
{
	while (parametr >= 2 * PI)
	{
		parametr -= 2 * PI;
	}
		
	double result = 0;
	int n = 2;
	double term = parametr;

	while (abs(term) - epsillon >= 0)
	{
		result += term;
		term *= -(parametr * parametr / n) / ((double)n + 1);
		n += 2;
	}

	return result;
}

double cosTh(double parametr, double epsillon)
{
	while (parametr >= 2 * PI)
	{
		parametr -= 2 * PI;
	}

	double result = 0;
	int n = 1;
	double term = 1;

	while (abs(term) - epsillon >= 0)
	{
		result += term;
		term *= -(parametr * parametr / n) / ((double)n + 1);
		n += 2;
	}

	return result;
}

double expTh(double parametr, double epsillon)
{
	double result = 1;
	double n = 1;
	double term = 1;

	while (abs(term) - epsillon > 0)
	{
		term *= (parametr / n);
		n++;
		result += term;
	}

	return result;
}


double** createMatrix(int size)
{
	double** arr = new double*[size];
	for (double** p = arr; p < (arr + size); p++)
	{
		*p = new double[size];
	}
	
	return arr;
}

void fillMatrixTh(double** arr, int size, double epsillon)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			arr[i][j] = (i == j) ? ((double)i + (double)j) * expTh((double)i + (double)j, epsillon) / (sinTh(2 * (double)j, epsillon) + 4) : (double)i - (double)j;
		}
	}
}

void fillMatrix(double** arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			arr[i][j] = (i == j) ? ((double)i + (double)j) * exp((double)i + (double)j) / (sin(2 * (double)j) + 4) : (double)i - (double)j;
		}
	}
}

void getMatrix(double** arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			cout.width(10);
			cout << arr[i][j] << " ";
		}

		cout << endl;
	}
}

double matrixMax(double* arr, int size)
{
	double max = DBL_MIN;
	for (int i = 0; i < size; i++)
	{
		if (arr[i] >= max)
			max = arr[i];
	}

	return max;
}

double matrixNorm(double** arr, int size)
{
	double* sumArr = new double[size] {0};
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			sumArr[i] += arr[j][i];
		}
	}

	return matrixMax(sumArr, size);
}

double matrixDiff(double** arrA, double** arrB, int size)
{
	return abs(matrixNorm(arrA, size) - matrixNorm(arrB, size));
}


int main()
{
	


}