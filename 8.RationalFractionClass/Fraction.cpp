#include "Fraction.h"
#include <iostream>

int Fraction::gcd(int lhs, int rhs) {
	
	if (lhs < 0)
		lhs = abs(lhs);
	if (rhs < 0)
		rhs = abs(rhs);

	if (lhs == 0)
		return rhs;
	if (rhs == 0)
		return lhs;
	
	return lhs > rhs ? gcd(lhs - rhs, rhs) : gcd(lhs, rhs - lhs);
}

Fraction::Fraction(const Fraction& fr) : numerator(fr.numerator), denominator(fr.denominator)
{
}

Fraction::Fraction(int num = 0, int den = 1)
{
	if (den == 0)
	{
		throw std::exception("Cannot divide by zero ");
	}

	if ((num < 0 && den < 0) || (num > 0 && den > 0))
	{
		this->numerator = abs(num);
		this->denominator = abs(den);
	}else if (den < 0 && num > 0)
	{
	this->numerator = -num;
	this->denominator = -den;
	}else
	{
		this->numerator = num;
		this->denominator = den;
	}
}

void Fraction::getFraction()
{
	std::cout << " " << this->numerator << "\n---\n" << " " << this->denominator << std::endl;
}

int& Fraction::getNumerator()
{
	return this->numerator;
}

int& Fraction::getDenominator()
{
	return this->denominator;
}

Fraction& Fraction::reduce()
{
	int commonDivisor = gcd(this->numerator, this->denominator);
	this->numerator /= commonDivisor;
	this->denominator /= commonDivisor;
	return *this;
}

Fraction Fraction::reduce(Fraction reduceble)
{
	int commonDivisor = gcd(reduceble.getNumerator(), reduceble.getDenominator());
	return Fraction(reduceble.getNumerator() / commonDivisor, reduceble.getDenominator() / commonDivisor);
}

Fraction Fraction::operator=(Fraction rhs)
{
	this->numerator = rhs.getNumerator();
	this->denominator = rhs.getDenominator();
	return *this;
}

Fraction Fraction::operator+(Fraction rhs)
{
	return Fraction(this->numerator * rhs.getDenominator() + rhs.getNumerator() * this->denominator, this->denominator * rhs.getDenominator()).reduce();
}

Fraction Fraction::operator-(Fraction rhs)
{
	return *this + Fraction(-rhs.getNumerator(), rhs.getDenominator());
}

Fraction Fraction::operator*(Fraction rhs)
{
	return Fraction(this->numerator * rhs.numerator, this->denominator * rhs.denominator).reduce();
}

Fraction Fraction::operator/(Fraction rhs)
{
	return Fraction(this->numerator * rhs.getDenominator(), this->denominator * rhs.getNumerator()).reduce();
}

bool Fraction::operator==(Fraction rhs)
{
	Fraction tempLHS = reduce(*this);
	Fraction tempRHS = reduce(rhs);

	return tempLHS.getNumerator() == tempRHS.getNumerator() && tempLHS.getDenominator() == tempRHS.getDenominator();
}

bool Fraction::operator>(Fraction rhs)
{
	Fraction tempLHS = reduce(*this);
	Fraction tempRHS = reduce(rhs);

	return (tempLHS.getNumerator() > tempRHS.getNumerator() && tempLHS.getDenominator() < tempRHS.getDenominator()) ||
		   (tempLHS.getNumerator() >= tempRHS.getNumerator() && tempLHS.getDenominator() < tempRHS.getDenominator()) || 
		   (tempLHS.getNumerator() > tempRHS.getNumerator() && tempLHS.getDenominator() <= tempRHS.getDenominator());
}

bool Fraction::operator>=(Fraction rhs)
{
	Fraction tempLHS = reduce(*this);
	Fraction tempRHS = reduce(rhs);

	return tempLHS.getNumerator() >= tempRHS.getNumerator() && tempLHS.getDenominator() <= tempRHS.getDenominator();
}

bool Fraction::operator<(Fraction rhs)
{
	Fraction tempLHS = reduce(*this);
	Fraction tempRHS = reduce(rhs);

	return (tempLHS.getNumerator() < tempRHS.getNumerator() && tempLHS.getDenominator() > tempRHS.getDenominator()) ||
		   (tempLHS.getNumerator() <= tempRHS.getNumerator() && tempLHS.getDenominator() > tempRHS.getDenominator()) ||
		   (tempLHS.getNumerator() < tempRHS.getNumerator() && tempLHS.getDenominator() >= tempRHS.getDenominator());
}

bool Fraction::operator<=(Fraction rhs)
{
	Fraction tempLHS = reduce(*this);
	Fraction tempRHS = reduce(rhs);

	return tempLHS.getNumerator() <= tempRHS.getNumerator() && tempLHS.getDenominator() >= tempRHS.getDenominator();
}
