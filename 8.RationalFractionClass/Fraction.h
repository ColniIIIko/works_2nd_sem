#pragma once
class Fraction
{
private:
	int numerator;
	int denominator;
	static int gcd(int, int);
	static Fraction reduce(Fraction);

public:
	Fraction(const Fraction&);
	Fraction(int num, int den);

	void getFraction();
	int& getNumerator();
	int& getDenominator();
	Fraction& reduce();

	Fraction operator+(Fraction);
	Fraction operator-(Fraction);
	Fraction operator*(Fraction);
	Fraction operator/(Fraction);
	Fraction operator=(Fraction);
	bool operator==(Fraction);
	bool operator>(Fraction);
	bool operator>=(Fraction);
	bool operator<(Fraction);
	bool operator<=(Fraction);
};



